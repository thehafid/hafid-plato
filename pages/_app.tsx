import React from 'react';

import "src/style/app.scss";

function MyApp({Component, pageProps}) {
  return (
    <React.Fragment>
      <Component {...pageProps} />
    </React.Fragment>
  );
}

export default MyApp;
