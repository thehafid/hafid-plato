import React from "react";

class Page extends React.Component {
  render() {
    return (
      <div className="container mt-5">
        <p>Hello, world!</p>
      </div>
    );
  }
}

export default Page;
