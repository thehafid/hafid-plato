import _ from "lodash";
import axios from "axios";

axios.defaults.withCredentials = true;

declare interface IResponse {
  data: any | null;
  status: number;
}

class HttpHelper {
  static async get(url: string): Promise<IResponse> {
    url = process.env.API_HOST + url;

    try {
      const {data, status} = await axios.get(url);

      return {
        data: data,
        status: status,
      };
    } catch (error) {
      const data = _.get(error, "response.data", null);
      const status = _.get(error, "response.status", 0);

      return {
        data: data,
        status: status,
      };
    }
  }
}

export default HttpHelper;
