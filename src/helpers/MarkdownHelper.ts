import * as marked from "marked";

class MarkdownHelper {
  static toHTML(input: string) {
    return marked(input);
  }
}

export default MarkdownHelper;
