import React from "react";

import MarkdownHelper from "src/helpers/MarkdownHelper";
import {LectureSchema} from "src/models/Lecture";

declare interface IProps {
  lecture: LectureSchema | null;
}

class LectureContent extends React.Component<IProps, any> {
  render() {
    const lecture = this.props.lecture;
    if (lecture === null) {
      return <section className="lecture-content"/>;
    }

    const html = MarkdownHelper.toHTML(lecture.content);

    return (
      <section className="lecture-content" dangerouslySetInnerHTML={{__html: html}}/>
    );
  }
}

export default LectureContent;
