export declare interface LectureSchema {
  id: number;
  course_id: number;
  title: string;
  content: string;
  position: number;
  created_at: string;
  updated_at: string;
}
