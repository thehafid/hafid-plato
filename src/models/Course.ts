import HttpHelper from "src/helpers/HttpHelper";

import {LectureSchema} from "./Lecture";

class Course {
  static async retrieveCourseDetails(slug: String): Promise<CourseSchema> {
    const {data, status} = await HttpHelper.get("/courses/" + slug);
    if (status !== 200) {
      return null;
    }

    return data;
  }
}

export default Course;

export declare interface CourseSchema {
  id: number;
  user_id: number;
  slug: string;
  title: string;
  is_public: boolean;
  created_at: string;
  updated_at: string;
  lectures: Array<LectureSchema>;
}
