import React from "react";

import {CourseSchema} from "src/models/Course";

declare interface IProps {
  course: CourseSchema;
  currentLecture: number;
  onChangeLecture: (next: number) => void;
}

class LectureNavigator extends React.Component<IProps, any> {
  constructor(props) {
    super(props);

    this.hasNextLecture = this.hasNextLecture.bind(this);
    this.hasPreviousLecture = this.hasPreviousLecture.bind(this);
    this.onClickNextLecture = this.onClickNextLecture.bind(this);
    this.onClickPreviousLecture = this.onClickPreviousLecture.bind(this);
  }

  hasNextLecture(): boolean {
    return this.props.currentLecture < this.props.course.lectures.length - 1
  }

  hasPreviousLecture(): boolean {
    return this.props.currentLecture > 0;
  }

  onClickNextLecture(): void {
    if (this.hasNextLecture() !== true) {
      return;
    }

    this.props.onChangeLecture(this.props.currentLecture + 1);
  }

  onClickPreviousLecture(): void {
    if (this.hasPreviousLecture() !== true) {
      return;
    }

    this.props.onChangeLecture(this.props.currentLecture - 1);
  }

  render() {
    return (
      <div className="lecture-navigator d-flex">
        <button className="btn btn-secondary mr-auto" onClick={this.onClickPreviousLecture}
                disabled={!this.hasPreviousLecture()}>
          Back
        </button>

        <button className="btn btn-primary ml-auto" onClick={this.onClickNextLecture}
                disabled={!this.hasNextLecture()}>
          Next
        </button>
      </div>
    );
  }
}

export default LectureNavigator;
