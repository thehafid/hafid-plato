import React from "react";

import {CourseSchema} from "src/models/Course";
import {LectureSchema} from "src/models/Lecture";

declare interface IProps {
  course: CourseSchema;
  lecture: LectureSchema | null;
  onToggleSidebar: () => void;
}

class SmallHeader extends React.Component<IProps, any> {
  render() {
    const {course, lecture} = this.props;

    if (lecture === null) {
      return <React.Fragment/>;
    }

    return (
      <div className="lecture-header sm">
        <div className="sidebar-toggle" onClick={this.props.onToggleSidebar}>
          <i className="fas fa-bars fa-fw"/>
        </div>

        <p className="course-title">
          {course.title}
        </p>
      </div>
    );
  }
}

export default SmallHeader;
