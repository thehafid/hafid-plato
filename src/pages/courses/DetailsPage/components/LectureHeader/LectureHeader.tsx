import React from "react";

import {CourseSchema} from "src/models/Course";
import {LectureSchema} from "src/models/Lecture";

import LargeHeader from "./LargeHeader";
import SmallHeader from "./SmallHeader";

declare interface IProps {
  course: CourseSchema;
  lecture: LectureSchema | null;
  onToggleSidebar: () => void;
}

class LectureHeader extends React.Component<IProps, any> {
  render() {
    const {course, lecture} = this.props;

    if (lecture === null) {
      return <React.Fragment/>;
    }

    return (
      <React.Fragment>
        <LargeHeader course={course} lecture={lecture}/>
        <SmallHeader course={course} lecture={lecture} onToggleSidebar={this.props.onToggleSidebar}/>
      </React.Fragment>
    );
  }
}

export default LectureHeader;
