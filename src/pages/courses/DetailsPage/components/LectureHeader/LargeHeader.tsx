import React from "react";

import {CourseSchema} from "src/models/Course";
import {LectureSchema} from "src/models/Lecture";

declare interface IProps {
  course: CourseSchema;
  lecture: LectureSchema | null;
}

class LargeHeader extends React.Component<IProps, any> {
  render() {
    const {course, lecture} = this.props;

    if (lecture === null) {
      return <React.Fragment/>;
    }

    return (
      <div className="lecture-header lg">
        <h6 className="course-title">{course.title}</h6>
        <h2 className="lecture-title">{lecture.title}</h2>
      </div>
    );
  }
}

export default LargeHeader;
