import Link from "next/link";
import React from "react";

import {CourseSchema} from "src/models/Course";

declare interface IProps {
  course: CourseSchema;
  currentLecture: number;
  onChangeLecture: (next: number) => void;
  onToggleSidebar: () => void;
  isExpanded: boolean;
}

class Sidebar extends React.Component<IProps, any> {
  render() {
    const self = this;
    const {course, currentLecture} = this.props;

    const classNames = ["sidebar"];
    if (this.props.isExpanded) {
      classNames.push("expanded")
    }

    return (
      <React.Fragment>
        {this.props.isExpanded && <div className="sidebar--backdrop" onClick={this.props.onToggleSidebar}/>}

        <div className={classNames.join(" ")}>
          <div className="sidebar--brand">
            <img alt="" src={"/assets/images/logo.png"}/>

            <h2>
              <Link href={"/"}>
                <a href="#">Hafid</a>
              </Link>
            </h2>
          </div>

          <div className="sidebar--heading">
            Contents
          </div>

          {course.lectures.map(function (lecture, index) {
            const classNames = ["sidebar--item"];
            if (index === currentLecture) {
              classNames.push("active");
            }

            const onClick = function (e) {
              e.preventDefault();
              self.props.onChangeLecture(index);
              self.props.onToggleSidebar();
            }

            return (
              <div key={index} className={classNames.join(" ")} onClick={onClick}>
                {lecture.title}
              </div>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}

export default Sidebar;
