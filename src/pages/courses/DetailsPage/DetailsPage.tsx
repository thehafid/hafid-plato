import _ from "lodash";
import Error from "next/error";
import React from "react";

import Course, {CourseSchema} from "src/models/Course";
import LectureContent from "src/components/LectureContent/LectureContent";
import {LectureSchema} from "src/models/Lecture";

import LectureHeader from "./components/LectureHeader/LectureHeader";
import LectureNavigator from "./components/LectureNavigator/LectureNavigator";
import Sidebar from "./components/Sidebar/Sidebar";

declare interface IProps {
  course: CourseSchema | null;
}

declare interface IState {
  currentLecture: number;
  isSidebarExpanded: boolean;
}

class DetailsPage extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      currentLecture: 0,
      isSidebarExpanded: false,
    };

    this.getCurrentLecture = this.getCurrentLecture.bind(this);
    this.onChangeLecture = this.onChangeLecture.bind(this);
    this.onToggleSidebar = this.onToggleSidebar.bind(this)
  }

  getCurrentLecture(): LectureSchema {
    if (this.props.course.lectures.length > this.state.currentLecture) {
      return this.props.course.lectures[this.state.currentLecture];
    }

    return null;
  }

  onChangeLecture(next: number): void {
    this.setState({
      currentLecture: next,
    });

    window.scrollTo(0, 0);
  }

  onToggleSidebar() {
    this.setState({
      isSidebarExpanded: !this.state.isSidebarExpanded,
    });
  }

  render() {
    const course = this.props.course;
    if (course === null) {
      return <Error statusCode={404}/>
    }

    return (
      <section id="courses-details">
        <Sidebar
          course={this.props.course}
          currentLecture={this.state.currentLecture}
          onChangeLecture={this.onChangeLecture}
          onToggleSidebar={this.onToggleSidebar}
          isExpanded={this.state.isSidebarExpanded}/>

        <div className="course--content">
          <div className="container">
            <LectureHeader
              course={this.props.course}
              lecture={this.getCurrentLecture()}
              onToggleSidebar={this.onToggleSidebar}/>

            <div className="card mb-5">
              <div className="card-body">
                <LectureContent lecture={this.getCurrentLecture()}/>
              </div>
            </div>

            <LectureNavigator
              course={this.props.course}
              currentLecture={this.state.currentLecture}
              onChangeLecture={this.onChangeLecture}/>
          </div>
        </div>
      </section>
    );
  }
}

export default DetailsPage;

export async function getServerSideProps(context) {
  return {
    props: {
      course: await Course.retrieveCourseDetails(_.get(context, "params.slug")),
    }
  };
}
